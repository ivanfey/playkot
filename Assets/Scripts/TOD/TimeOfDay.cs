﻿using UnityEngine;
using UnityEngine.Events;

namespace TOD
{
    public class TimeOfDay : MonoBehaviour
    {
        [System.Serializable]
        public class OnHourChangedEvent : UnityEvent<int> { }

        private const string _saveTimeParamName = "TimeOfDay";

        public static OnHourChangedEvent OnHourChanged = new OnHourChangedEvent();

        private const float _secondsInDay = 86400f;
        private const float _dayDuration = 30f;

        private static float _actualTime = 0f;
        private static int _actualTimeInHours = 0;
        private static int _actualTimeInMinutes = 0;

        private void OnEnable()
        {
            _actualTime = PlayerPrefs.GetFloat(_saveTimeParamName, 0f);
        }

        private void OnDisable()
        {
            PlayerPrefs.SetFloat(_saveTimeParamName, _actualTime);
        }

        private void Update()
        {
            _actualTime += Time.deltaTime;
            if (_actualTime >= _dayDuration)
                _actualTime = 0f;

            _actualTimeInMinutes = (Mathf.FloorToInt((_secondsInDay * _actualTime / _dayDuration) / 60) % 60);

            var tempTimeInHours = Mathf.FloorToInt((_secondsInDay * _actualTime / _dayDuration) / 60 / 60);
            if (tempTimeInHours != _actualTimeInHours)
                OnHourChanged.Invoke(tempTimeInHours);
            _actualTimeInHours = tempTimeInHours;
        }

        public static int GetHours()
        {
            return _actualTimeInHours;
        }

        public static int GetMinutes()
        {
            return _actualTimeInMinutes;
        }

        public static void SetActialTime(float newActualTime)
        {
            _actualTime = newActualTime;
        }
    }
}

