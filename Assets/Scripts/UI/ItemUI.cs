﻿using UnityEngine;
using Core.Inventory;
using UnityEngine.UI;

namespace UI
{
    public class ItemUI : MonoBehaviour
    {
        [SerializeField]
        private Text _productNameTextArea = null;
        [SerializeField]
        private Text _productDescriptionTextArea = null;
        [SerializeField]
        private Text _productCountTextArea = null;
        [SerializeField]
        private Button _dropButton = null;
        [SerializeField]
        private Button _sellButton = null;

        private uint _count = 0;

        public void Init(InventoryItem selfInventory, InventoryItem opponentInventory, ProductItem item, uint count)
        {
            var catalogItem = Core.Market.MarketManager.GetProductItemById(item.CatalogId);
            _productNameTextArea.text = catalogItem.Name;
            _productDescriptionTextArea.text = catalogItem.Description;
            _productCountTextArea.text = count == 1 ? "" : count.ToString();
            _count = count;

            _dropButton.gameObject.SetActive(true);
            _dropButton.onClick.RemoveAllListeners();
            _dropButton.onClick.AddListener(()=> 
            {
                InventoryManager.RemoveItemFromInventory(selfInventory, item);
            });

            _sellButton.gameObject.SetActive(false);
            if(opponentInventory != null)
            {
                _sellButton.gameObject.SetActive(true);
                _sellButton.onClick.RemoveAllListeners();
                _sellButton.onClick.AddListener(()=> 
                {
                    InventoryManager.PurchaseItem(selfInventory, opponentInventory, item);
                });
            }
        }

        public void AddCount()
        {
            _count++;
            _productCountTextArea.text = _count.ToString();
        }
    }
}

