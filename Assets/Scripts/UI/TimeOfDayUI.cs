﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TimeOfDayUI : MonoBehaviour
    {
        [SerializeField]
        private Text _timeOfDayTextArea = null;

        private void Update()
        {
            var mm = TOD.TimeOfDay.GetMinutes().ToString("00");
            var hh = TOD.TimeOfDay.GetHours().ToString("00");
            _timeOfDayTextArea.text = (hh + ":" + mm);
        }
    }
}

