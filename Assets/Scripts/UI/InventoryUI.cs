﻿using UnityEngine;
using Core.Inventory;
using UnityEngine.UI;
using System.Collections.Generic;

namespace UI
{
    public class InventoryUI : MonoBehaviour
    {
        [SerializeField]
        private string _inventoryItemId = "Player";
        [SerializeField]
        private Text _moneyTextArea = null;
        [SerializeField]
        private Text _inventoryItemIdTextArea = null;
        [SerializeField]
        private GameObject _defaultItemGO = null;
        [SerializeField]
        private string _opponentInventoryId = null;

        private List<ItemUI> _allItemUI = new List<ItemUI>();
        private List<ItemUI> _allItemUIDisabled = new List<ItemUI>();

        private void OnEnable()
        {
            InventoryManager.OnInventoryChanged.AddListener(Init);
        }

        private void OnDisable()
        {
            InventoryManager.OnInventoryChanged.RemoveListener(Init);
        }

        private void Start()
        {
            Init(_inventoryItemId);
        }

        public void Init(string inventryItemId)
        {
            if (_inventoryItemId != inventryItemId)
                return;

            var inventoryItem = AbstractInventory.GetInventoryItemById(_inventoryItemId);
            _moneyTextArea.text = inventoryItem.Money.ToString();
            _inventoryItemIdTextArea.text = _inventoryItemId;

            for (int i = 0; i < _allItemUI.Count; i++)
            {
                _allItemUI[i].gameObject.SetActive(false);
                _allItemUIDisabled.Add(_allItemUI[i]);
            }
            _allItemUI.Clear();

            var alreadyExistCatalogItemIds = new Dictionary<string, ItemUI>();
            foreach (var pair in inventoryItem.ProductItems)
            {
                if (alreadyExistCatalogItemIds.ContainsKey(pair.Value.CatalogId))
                {
                    alreadyExistCatalogItemIds[pair.Value.CatalogId].AddCount();
                    continue;
                }

                ItemUI itemUI = null;
                if (_allItemUIDisabled.Count == 0)
                {
                    var itemUIGO = Instantiate(_defaultItemGO, _defaultItemGO.transform.parent);
                    itemUIGO.SetActive(true);
                    itemUI = itemUIGO.GetComponent<ItemUI>();
                }
                else
                {
                    itemUI = _allItemUIDisabled[0];
                    itemUI.gameObject.SetActive(true);
                    _allItemUIDisabled.RemoveAt(0);
                }

                var opponentInventory = AbstractInventory.GetInventoryItemById(_opponentInventoryId);
                itemUI.Init(inventoryItem, opponentInventory, pair.Value, 1);
                alreadyExistCatalogItemIds.Add(pair.Value.CatalogId, itemUI);

                _allItemUI.Add(itemUI);
            }
        }
    }
}

