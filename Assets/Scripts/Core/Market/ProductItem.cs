﻿namespace Core.Market
{
    [System.Serializable]
    public struct ProductItem
    {
        public string Id;
        public string Name;
        public string Description;
        public uint Cost;
    }
}