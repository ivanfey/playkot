﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Market
{
    [System.Serializable]
    public struct MarketItem
    {
        public string Id;
        public string Name;

        public List<string> SetItemIds;

        public string GetSetItemByRandom()
        {
            return SetItemIds[Random.Range(0, SetItemIds.Count)];
        }
    }
}