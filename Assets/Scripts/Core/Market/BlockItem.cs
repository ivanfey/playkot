﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Market
{
    [System.Serializable]
    public struct BlockItem
    {
        [System.Serializable]
        public struct BlockEntity
        {
            public string Name;
            public string ProductItemId;
            public uint Count;
            public uint Weight;
        }

        public string Id;
        public string Name;

        public List<BlockEntity> BlockEntities;

        public BlockEntity GetBlockEntityByRandom()
        {
            return BlockEntities[Random.Range(0, BlockEntities.Count)];
        }

        public bool GetBlockEntityByWeight(ref BlockEntity blockEntity)
        {
            uint allWeight = 0;
            for(int i = 0; i < BlockEntities.Count; i++)
            {
                allWeight += BlockEntities[i].Weight;
            }

            uint accumulatedWeight = 0;
            var randomValue = Random.Range(0, allWeight);
            for (int i = 0; i < BlockEntities.Count; i++)
            {
                accumulatedWeight += BlockEntities[i].Weight;
                if (randomValue > accumulatedWeight)
                    continue;

                blockEntity = BlockEntities[i];
                return true; 
            }

            return false;
        }
    }
}