﻿using System.Collections.Generic;

namespace Core.Market
{
    [System.Serializable]
    public struct SetItem
    {
        [System.Serializable]
        public struct SetEntity
        {
            public string Name;
            public string BlockItemId;
        }

        public string Id;
        public string Name;
        public List<SetEntity> SetEntities;
    }
}