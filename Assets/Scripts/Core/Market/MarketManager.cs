﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Market
{
    public class MarketManager : MonoBehaviour
    {
        public static UnityEvent OnInitDone = new UnityEvent();

        [SerializeField]
        private string _productsConfigPath = "Configs/Products";
        [SerializeField]
        private string _blocksConfigPath = "Configs/Blocks";
        [SerializeField]
        private string _setsConfigPath = "Configs/Sets";
        [SerializeField]
        private string _marketsConfigPath = "Configs/Markets";

        private static Dictionary<string, ProductItem> _productItems = new Dictionary<string, ProductItem>();
        private static Dictionary<string, BlockItem> _blockItems = new Dictionary<string, BlockItem>();
        private static Dictionary<string, SetItem> _setItems = new Dictionary<string, SetItem>();
        private static Dictionary<string, MarketItem> _marketItems = new Dictionary<string, MarketItem>();

        public static bool InitDone = false;

        public static ProductItem GetProductItemById(string id)
        {
            return _productItems[id];
        }

        public static BlockItem GetBlockItemById(string id)
        {
            return _blockItems[id];
        }

        public static SetItem GetSetItemById(string id)
        {
            return _setItems[id];
        }

        public static MarketItem GetMarketItemById(string id)
        {
            return _marketItems[id];
        }

        private void GetProductsConfig()
        {
            var itemsList = GetItemsList<ProductItem>(_productsConfigPath);
            _productItems = new Dictionary<string, ProductItem>();
            for (int i = 0; i < itemsList.Count; i++)
            {
                _productItems.Add(itemsList[i].Id, itemsList[i]);
            }
        }

        private void GetBlocksConfig()
        {
            var itemsList = GetItemsList<BlockItem>(_blocksConfigPath);
            _blockItems = new Dictionary<string, BlockItem>();
            for (int i = 0; i < itemsList.Count; i++)
            {
                _blockItems.Add(itemsList[i].Id, itemsList[i]);
            }
        }

        private void GetSetsConfig()
        {
            var itemsList = GetItemsList<SetItem>(_setsConfigPath);
            _setItems = new Dictionary<string, SetItem>();
            for (int i = 0; i < itemsList.Count; i++)
            {
                _setItems.Add(itemsList[i].Id, itemsList[i]);
            }
        }

        private void GetMarketsConfig()
        {
            var itemsList = GetItemsList<MarketItem>(_marketsConfigPath);
            _marketItems = new Dictionary<string, MarketItem>();
            for (int i = 0; i < itemsList.Count; i++)
            {
                _marketItems.Add(itemsList[i].Id, itemsList[i]);
            }
        }

        private List<T> GetItemsList<T>(string configPath)
        {
            var tempDict = new Dictionary<string, T>();
            var jsonConfig = Resources.Load<TextAsset>(configPath).text;
            return JsonConvert.DeserializeObject<List<T>>(jsonConfig);
        }

        private void Awake()
        {
            InitDone = false;
            Init();
        }

        public void Init()
        {
            GetProductsConfig();
            GetBlocksConfig();
            GetSetsConfig();
            GetMarketsConfig();

            OnInitDone.Invoke();
            InitDone = true;
        }
    }
}