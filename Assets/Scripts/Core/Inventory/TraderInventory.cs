﻿using System;
using System.Collections;
using System.Collections.Generic;
using TOD;
using UnityEngine;

namespace Core.Inventory
{
    public class TraderInventory : AbstractInventory
    {
        private const int _startMoney = 1000;

        [SerializeField]
        private string _marketId = "MarketId_0";
        [SerializeField]
        private List<int> _updateHours = new List<int>();

        public override void OnEnable()
        {
            base.OnEnable();
            TimeOfDay.OnHourChanged.AddListener(CheckForUpdate);
        }

        public override void OnDisable()
        {
            base.OnDisable();
            TimeOfDay.OnHourChanged.RemoveListener(CheckForUpdate);
        }

        public override void SetStartParams()
        {
            base.SetStartParams();
            UpdateInventory();
        }

        private void CheckForUpdate(int hours)
        {
            if (_updateHours.Contains(hours))
                UpdateInventory();
        }

        private void UpdateInventory()
        {
            _inventoryItem.Money = _startMoney;
            _inventoryItem.ProductItems.Clear();

            var marketItem = Market.MarketManager.GetMarketItemById(_marketId);
            var setItemId = marketItem.GetSetItemByRandom();
            var setItem = Market.MarketManager.GetSetItemById(setItemId);
            for (int i = 0; i < setItem.SetEntities.Count; i++)
            {
                var blockItemId = setItem.SetEntities[i].BlockItemId;
                var blockItem = Market.MarketManager.GetBlockItemById(blockItemId);

                var blockEntity = new Market.BlockItem.BlockEntity();
                if (!blockItem.GetBlockEntityByWeight(ref blockEntity))
                    Debug.LogError("Something wrong");
                
                for (int j = 0; j < blockEntity.Count; j++)
                {
                    var marketProductItem = Market.MarketManager.GetProductItemById(blockEntity.ProductItemId);
                    var tempItemId = _marketId + "_" + i + "_" + j + "_" + DateTime.UtcNow.ToString();
                    var inventoryProductItem = new ProductItem { Id = tempItemId, CatalogId = marketProductItem.Id };
                    if (!InventoryManager.AddItemInInventory(_inventoryItem, inventoryProductItem, false))
                        Debug.LogError("Something wrong");
                }
            }

            InventoryManager.OnInventoryChanged.Invoke(_inventoryItem.Id);

            Debug.Log("InventoryUpdate!");
        }
    }
}

