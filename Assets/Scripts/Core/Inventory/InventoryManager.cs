﻿using UnityEngine;
using UnityEngine.Events;

namespace Core.Inventory
{
    public class InventoryManager
    {
        [System.Serializable]
        public class OnInventoryChangedEvent : UnityEvent<string> { }

        public static OnInventoryChangedEvent OnInventoryChanged = new OnInventoryChangedEvent();

        public static bool AddItemInInventory(InventoryItem inventory, ProductItem item, bool invokeChangedEvent = true)
        {
            if (inventory.ProductItems.ContainsKey(item.Id))
            {
                Debug.LogError("This item is already in inventory");
                return false;
            }

            inventory.ProductItems.Add(item.Id, item);

            if (invokeChangedEvent)
                OnInventoryChanged.Invoke(inventory.Id);

            return true;
        }

        public static bool RemoveItemFromInventory(InventoryItem inventory, ProductItem item, bool invokeChangedEvent = true)
        {
            if (!inventory.ProductItems.ContainsKey(item.Id))
            {
                Debug.LogError("There is no item in inventory");
                return false;
            }

            inventory.ProductItems.Remove(item.Id);

            if (invokeChangedEvent)
                OnInventoryChanged.Invoke(inventory.Id);

            return true;
        }

        public static bool PurchaseItem(InventoryItem sellerInventory, InventoryItem buyerInventory, ProductItem item)
        {
            if (!sellerInventory.ProductItems.ContainsKey(item.Id))
            {
                Debug.LogError("The seller does not have this product");
                return false;
            }

            var catalogItem = Market.MarketManager.GetProductItemById(item.CatalogId);
            if (catalogItem.Cost > buyerInventory.Money)
            {
                Debug.LogError("The buyer does not have enough money");
                return false;
            }

            buyerInventory.Money -= catalogItem.Cost;
            sellerInventory.Money += catalogItem.Cost;
            if (RemoveItemFromInventory(sellerInventory, item) && AddItemInInventory(buyerInventory, item))
                return true;

            Debug.LogError("Something wrong");
            return false;
        }
    }
}