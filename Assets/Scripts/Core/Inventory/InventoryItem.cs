﻿using System.Collections.Generic;

namespace Core.Inventory
{
    [System.Serializable]
    public class InventoryItem
    {
        public string Id = null;
        public uint Money = 0;
        public Dictionary<string, ProductItem> ProductItems = new Dictionary<string, ProductItem>();
    }
}