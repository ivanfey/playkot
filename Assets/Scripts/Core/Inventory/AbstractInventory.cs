﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Inventory
{
    public class AbstractInventory : MonoBehaviour
    {
        private const string _saveInventoryParamName = "Inventory";

        private static Dictionary<string, InventoryItem> _allInventoryItems = new Dictionary<string, InventoryItem>();

        [SerializeField]
        protected InventoryItem _inventoryItem = null;

        public virtual void OnEnable()
        {
            Load();
        }

        public virtual void OnDisable()
        {
            Save();
        }

        public static InventoryItem GetInventoryItemById(string id)
        {
            return _allInventoryItems[id];
        }

        public virtual void SetStartParams()
        {

        }

        private void Save()
        {
            var savedInventoriesText = PlayerPrefs.GetString(_saveInventoryParamName, JsonConvert.SerializeObject(new Dictionary<string, InventoryItem>()));
            var savedInventories = JsonConvert.DeserializeObject<Dictionary<string, InventoryItem>>(savedInventoriesText);
            if (savedInventories.ContainsKey(_inventoryItem.Id))
                savedInventories[_inventoryItem.Id] = _inventoryItem;
            else
                savedInventories.Add(_inventoryItem.Id, _inventoryItem);
            PlayerPrefs.SetString(_saveInventoryParamName, JsonConvert.SerializeObject(savedInventories));

            if (_allInventoryItems.ContainsKey(_inventoryItem.Id))
                _allInventoryItems.Remove(_inventoryItem.Id);
        }

        private void Load()
        {
            var savedInventoriesText = PlayerPrefs.GetString(_saveInventoryParamName, JsonConvert.SerializeObject(new Dictionary<string, InventoryItem>()));
            var savedInventories = JsonConvert.DeserializeObject<Dictionary<string, InventoryItem>>(savedInventoriesText);
            if (savedInventories.ContainsKey(_inventoryItem.Id))
            {
                _inventoryItem = savedInventories[_inventoryItem.Id];

                if (!_allInventoryItems.ContainsKey(_inventoryItem.Id))
                    _allInventoryItems.Add(_inventoryItem.Id, _inventoryItem);
            }
            else
            {
                if (!_allInventoryItems.ContainsKey(_inventoryItem.Id))
                    _allInventoryItems.Add(_inventoryItem.Id, _inventoryItem);

                SetStartParams();
            } 
        }
    }
}

