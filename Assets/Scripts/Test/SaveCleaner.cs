﻿using UnityEngine;

namespace Test
{
    public class SaveCleaner : MonoBehaviour
    {
        [ContextMenu ("Clear all save")]
        public void ClearAllSave()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}

